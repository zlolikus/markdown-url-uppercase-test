# markdown-url-uppercase-test

## Introduction (example)

The W25Q256JV (256M-bit) Serial Flash memory provides a storage solution for systems with limited
space, pins and power. The 25Q series offers flexibility and performance well beyond ordinary Serial Flash
devices. They are ideal for code shadowing to RAM, executing code directly from Dual/Quad SPI (XIP)
and storing voice, text and data. The device operates on a single 2.7V to 3.6V power supply with current
consumption as low as 1µA for power-down. All devices are offered in space-saving packages.
The W25Q256JV array is organized into 131,072 programmable pages of 256-bytes each. Up to 256
bytes can be programmed at a time. Pages can be erased in groups of 16 (4KB sector erase), groups of
128 (32KB block erase), groups of 256 (64KB block erase) or the entire chip (chip erase). The
W25Q256JV has 8,192 erasable sectors and 512 erasable blocks respectively. The small 4KB sectors
allow for greater flexibility in applications that require data and parameter storage.
The W25Q256JV supports the standard Serial Peripheral Interface (SPI), Dual/Quad I/O SPI: Serial
Clock, Chip Select, Serial Data I/O0 (DI), I/O1 (DO), I/O2, and I/O3. SPI clock frequencies of W25Q256JV
of up to 133MHz are supported allowing equivalent clock rates of 266MHz (133MHz x 2) for Dual I/O and
532MHz (133MHz x 4) for Quad I/O when using the Fast Read Dual/Quad I/O. These transfer rates can
outperform standard Asynchronous 8 and 16-bit Parallel Flash memories.
Additionally, the device supports JEDEC standard manufacturer and device ID and SFDP Register, a 64-
bit Unique Serial Number and three 256-bytes Security Registers.

The Read Data with 4-Byte Address instruction is similar to the Read Data (03h) instruction. Instead of 24-
bit address, 32-bit address is needed following the instruction code 13h. No matter the device is operating
in 3-Byte Address Mode or 4-byte Address Mode, the Read Data with 4-Byte Address instruction will
always require 32-bit address to access the entire 256Mb memory.
The Read Data with 4-Byte Address instruction sequence is shown in Figure 15. If this instruction is issued
while an Erase, Program or Write cycle is in process (BUSY=1) the instruction is ignored and will not have
any effects on the current cycle. The Read Data with 4-Byte Address instruction allows clock rates from
D.C. to a maximum of fR (see AC Electrical Characteristics).

## Getting started

Short instruction Краткая инструкция
------------------

The Read Data instruction allows one or more data bytes to be sequentially read from the memory. The
instruction is initiated by driving the /CS pin low and then shifting the instruction code “03h” followed by a
32/24-bit address (A31/A23-A0) into the DI pin. The code and address bits are latched on the rising edge
of the CLK pin. After the address is received, the data byte of the addressed memory location will be
shifted out on the DO pin at the falling edge of CLK with most significant bit (MSB) first. The address is
automatically incremented to the next higher address after each byte of data is shifted out allowing for a
continuous stream of data. This means that the entire memory can be accessed with a single instruction
as long as the clock continues. The instruction is completed by driving /CS high.

Краткая инструкция
------------------

The Read Data with 4-Byte Address instruction is similar to the Read Data (03h) instruction. Instead of 24-
bit address, 32-bit address is needed following the instruction code 13h. No matter the device is operating
in 3-Byte Address Mode or 4-byte Address Mode, the Read Data with 4-Byte Address instruction will
always require 32-bit address to access the entire 256Mb memory.
The Read Data with 4-Byte Address instruction sequence is shown in Figure 15. If this instruction is issued
while an Erase, Program or Write cycle is in process (BUSY=1) the instruction is ignored and will not have
any effects on the current cycle. The Read Data with 4-Byte Address instruction allows clock rates from
D.C. to a maximum of fR (see AC Electrical Characteristics).

The Read Data with 4-Byte Address instruction is similar to the Read Data (03h) instruction. Instead of 24-
bit address, 32-bit address is needed following the instruction code 13h. No matter the device is operating
in 3-Byte Address Mode or 4-byte Address Mode, the Read Data with 4-Byte Address instruction will
always require 32-bit address to access the entire 256Mb memory.
The Read Data with 4-Byte Address instruction sequence is shown in Figure 15. If this instruction is issued
while an Erase, Program or Write cycle is in process (BUSY=1) the instruction is ignored and will not have
any effects on the current cycle. The Read Data with 4-Byte Address instruction allows clock rates from
D.C. to a maximum of fR (see AC Electrical Characteristics).
